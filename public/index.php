<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
// เซ็ตเวลา ไม่งั้นเวลาเพี้ยนไม่ใช่ของประเทศไทย
date_default_timezone_set("Asia/Bangkok");



require '../vendor/autoload.php';

require '../config/config.php';

$config = [
    'settings' => [
        'displayErrorDetails' => true,
        // 'routerCacheFile' => true,
        // น่าจะเป็นการใช้log debug
        'logger' => [
            'name' => 'slim-app',
            'level' => Monolog\Logger::DEBUG,
            'path' => __DIR__ . '/../logs/app.log',
        ],
    ],
];

$app = new \Slim\App($config);

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', 'http://localhost:8080')
            // ถ้าใช้ allow Credentialsต้องระบุ เป็นhttpsตัวๆเลยไม่งั้นมันไม่ให้ใช้
            // ->withHeader('Access-Control-Allow-Origin', 'https://www.betonghospital.net')
            // ตัวนี้แหละต้องใช้ทั้ง2ฝั่งเพื่อให้sessionใช้ได้ หาอยู่เป็นเดือน
            ->withHeader('Access-Control-Allow-Credentials', 'true')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});



require '../routes/list_news.php';
require '../routes/check_login.php';
require '../routes/submit_news.php';

$app->run();

