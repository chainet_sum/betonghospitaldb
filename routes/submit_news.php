<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Http\UploadedFile;

// สร้าง ตัวแปรเรียกใช้ container ตั้งต้นไว้เลย

$container = $app->getContainer();

// ให้ตัวแปรเป็นarray ที่มีค่า diractory ที่จะเก็บไฟล์ upload

$container['upload_directory'] ='../news/';

$app->post('/api/submitnews' , function(Request $request, Response $response){
    // กำหนด dateในตัวแปร
    $date = date("d-m-Y");
    // เรียก ตัวแปร diractory เข้ามา
    $directory = $this->get('upload_directory');
    // ตัวแปรสร้าง request รับค่าที่ส่งมา
    $parameters = $request->getParsedBody();
    // ตัวแปรสร้าง request รับค่าไฟล์ที่อัพโหลดมา
    $uploadedFiles = $request->getUploadedFiles();
    // รับค่า name1โดยการเรียกใช้ ตัวแปร parameters
    $parameter = $parameters['name1'];
    
    // $Type = $parameters['Type1']; อนาคตเผื่อมี ข่าวหลายแบบ
    // เชื่อมฐาน
    $db = new db();
    $db = $db->connect();
    // เช็คว่า ไฟล์อัพโหลด ว่างไหม ว่างคือแสดงว่าไม่ได้อัพไฟล์มา
    if (empty($uploadedFiles['file1'])) {
        // ก็ให้ทำการ insert ปกติ
        $sql = "INSERT INTO news (Date,Nname, Type,createby)
        VALUES ('$date','$parameter','A','$_SESSION[username]')";
        $db->exec($sql);
        echo "บันทึกข้อมูลลงฐานข้อมูลเรียบร้อยแล้ว";
        $db = null;
        $response->withStatus(200);
        
    }else{
        // ถ้าอัพโหลดมีไฟล์ ก็เช็คอีกทีว่าไฟล์นั้น ถูกต้องไหม ถึงให้ไปต่อ
        $uploadedFile = $uploadedFiles['file1'];
        if($uploadedFile->getError() === UPLOAD_ERR_OK) {
        // ถ้าถูกก็เขียนinsert และ โยกไฟล์อัพโหลดไปยัง diractoryที่ตั้งไว้
        $filename = moveUploadedFile($directory, $uploadedFile);
        $sql = "INSERT INTO news (Date,Nname,Path, Type,createby)
        VALUES ('$date','$parameter','$filename','A','$_SESSION[username]')";
        $db->exec($sql);
        $db = null;
        $response->write('uploaded ' . $filename . '<br/>');
        $response->withStatus(200);

       }else {
        $response->withStatus(400);
       }
    }
});

// เหมือนกันกับข้างบน ของ psoแค่เปลี่ยนที่เก็บไลฟ์แต่ใช้ฐานเดียวกัน
$container['upload_directory3'] ='../purchase/';
$app->post('/api/submitpurchase' , function(Request $request, Response $response){
    $date = date("d-m-Y");
    $directory = $this->get('upload_directory3');
    $parameters = $request->getParsedBody();
    $uploadedFiles = $request->getUploadedFiles();
    $parameter = $parameters['name3'];
    $db = new db();
    $db = $db->connect();
    
    if (empty($uploadedFiles['file3'])) {
        
        $sql = "INSERT INTO news (Date,Nname, Type,createby)
        VALUES ('$date','$parameter','C','$_SESSION[username]')";
        $db->exec($sql);
        echo "บันทึกข้อมูลลงฐานข้อมูลเรียบร้อยแล้ว";
        $db = null;
        $response->withStatus(200);
        
    }else{

        $uploadedFile = $uploadedFiles['file3'];
        if($uploadedFile->getError() === UPLOAD_ERR_OK) {
        
        $filename = moveUploadedFile($directory, $uploadedFile);
        $sql = "INSERT INTO news (Date,Nname,Path, Type,createby)
        VALUES ('$date','$parameter','$filename','C','$_SESSION[username]')";
        $db->exec($sql);
        $db = null;
        $response->write('uploaded ' . $filename . '<br/>');
        $response->withStatus(200);

       }else {
        $response->withStatus(400);
       }
    }
});

// อันนี้ita 


$container = $app->getContainer();
$container['upload_directory2'] ='../ita/';

$app->post('/api/submitita' , function(Request $request, Response $response){
    $date = date("d-m-Y");
    $directory = $this->get('upload_directory2');
    $parameters = $request->getParsedBody();
    $uploadedFiles = $request->getUploadedFiles();
    $parameter = $parameters['name2'];
    $Type = $parameters['Type2']; 
    // เชื่อมฐาน
    $db = new db();
    $db = $db->connect();
    // เอาค่า หัวข้อไปselectก่อนเช็คว่ามีไฟล์ไหมเด๊่ยวชื่อมันซ้ำ ไม่งั้น การแสดงผลในfront จะขึ้นerrorในconsole ว่ามีชื่อซ้ำเลยต้องเช็ค
    do {
        $product = null;
        $checkname = "SELECT label FROM ita Where label = '$parameter' AND Type = '$Type'";
        $stmt = $db->query($checkname);
        $product = $stmt->fetchAll(PDO::FETCH_OBJ);
        
       if ($product != null) {
        //    ถ้ามีชื่อซ้ำก็เติม . เข้าไปให้มัน 5555 ต่อๆไปเรื่อยๆ
           
           $parameter = $parameter.'.';
       }
    //    จากนั้นเริ่มการinsert
    } while ($product != null);
        // ต้องมีไฟล์อยู่แล้วไม่งั้นติดตั้งแต่front ละเช็คความถูกต้องไฟล์ฺเลย

        $uploadedFile = $uploadedFiles['file2'];
        if($uploadedFile->getError() === UPLOAD_ERR_OK) {
        // เรียกใช้ฟังก์ชั่นข้างล่าง เพื่ออัพโหลดไฟล์ก่อนจากนั้นเขียนinsert
        $filename = moveUploadedFile($directory, $uploadedFile);
        $sql = "INSERT INTO ita (Date,label,Path, Type,createby)
        VALUES ('$date','$parameter','$filename','$Type','$_SESSION[username]')";
        $db->exec($sql);
        $db = null;
        $response->write('uploaded ' . $filename . '<br/>');
        $response->withStatus(200);

       }else {
        $response->withStatus(400);
       }
    
});

// ฟังก์ชั่นอัพโหลดไฟล์
 function moveUploadedFile($directory, UploadedFile $uploadedFile){
 $extension = pathinfo($uploadedFile->getClientFilename(), 
 PATHINFO_EXTENSION);
 $basename = date("d-m-Y");
//  สร้างชื่อให้ไฟล์เพื่อเก็บ เอาวันที่มาต่อกับนามสกุล .ต่างๆ สัญลักษณ์ใส่ ตรง%s
 $filename = sprintf('%s.%0.8s', $basename, $extension);
//  สร้างไว้ให้มันนับ
 $loopnum = 0;
//  loopนี้เอาไว้เช็คกลัวมันเขียนชื่อไฟล์อัพโหลดซ้ำถ้าเป็นวันเดียวกัน ให้นับเลขไปเรื่อยๆยัดตามหลังชื่อไฟล์อัพโหลดเลยไม่ซ้ำแน่นอน5555
    while (file_exists($directory.$filename)) {
        $loopnum = $loopnum + 1;
        // สัญลักษณ์ _ใส่ตรงนั้นเลยในชื่อไลฟ์ก็จะมี _ขั้น
        $filename = sprintf('%s_%0.9s.%0.8s', $basename,$loopnum,$extension);
    }
    // จากนั้นย้ายไฟล์ไปยัง diractory
    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
    // รีเทรินชื่อไฟล์สุทธิไปใช้เก็บในฐาน
    return $filename;
}

?>