<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// Aคือเอกสารของpso
$app->get('/api/getnewsA' , function(Request $request, Response $response){

    $sql = "SELECT Date,Nname,Path FROM news WHERE Type='A' ORDER BY Id DESC";
    // เรียงตาม date ไม่ได้ เพราะมันจะเรียงเลข เราเลือกประเภทการเก็บกับชนิดของ utf-8ไม่ถูก จริงๆควรเก็บเ้ป็น date แล้วเอามาคัดออกเอานะ ไม่งั้นก็numberดีกว่า
    // $sql = "SELECT *,DATE_FORMAT(date,'%d/%m/%Y') AS Date FROM news WHERE Type='A' ORDER BY Date DESC";

    
        $db         = new db();
        $db         = $db->connect();
        $stmt       = $db->query($sql);
        $product    = $stmt->fetchAll(PDO::FETCH_OBJ);

        $db = null;
        return $response->withJson($product, 200);

});

// อันนี้ของจัดซื้อจัดจ้างแต่เก็บในmysqlรวมกัน เรียงจากล่าสุดไปนานสุด
$app->get('/api/getpurchase' , function(Request $request, Response $response){

    $sql = "SELECT Date,Nname,Path FROM news WHERE Type='C' ORDER BY Id DESC";

        $db         = new db();
        $db         = $db->connect();
        $stmt       = $db->query($sql);
        $product    = $stmt->fetchAll(PDO::FETCH_OBJ);

        $db = null;
        return $response->withJson($product, 200);
});

// อันนี้เผื่อในอนาคตในtabอื่น
// $app->get('/api/getnewsC' , function(Request $request, Response $response){

//     $sql = "SELECT * FROM news WHERE Type='C'";

//         $db         = new db();
//         $db         = $db->connect();
//         $stmt       = $db->query($sql);
//         $product    = $stmt->fetchAll(PDO::FETCH_OBJ);

//         $db = null;
//         return $response->withJson($product, 200);
// });

$app->get('/api/getita' , function(Request $request, Response $response){

    $sql = "SELECT Date,label,Path,Type FROM ita";
    
        $db         = new db();
        $db         = $db->connect();
        $stmt       = $db->query($sql);
        // fetchassocออกมาเป็นarray แบบมีkey valueเรียกคนละแบบกับobjectนะ
        $product    = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $db = null;

    //    ตัวอย่าง for ออกมาเป็น array
    //    for ($x = 1; $x <= 24 ; $x++) {
    //          $ita[$x] = array("label" => "EB$x",
    //                         "icon" => "view_headline",
    //                         "header" => "generic",
    //                         "body" => "toggle",
    //                         "children" => "$x");
    //     } 

    // ตัวอย่างการสร้าง object แบบไม่ต้องเขียนclassมารองรับก่อน แล้วยัด keyเข้าไป
        // $myCar = new stdClass();
        // $myCar->body = 'story';
        // $myCar->header = 'generic';
        // $myCar->label = '111111111111';
        // $myCar->Path = 'eiei';
        // $myCar->story = '55555555';

        
// ตัวอย่าง เอาobject ไปยัดในarrayอีกทีนึง
        // $cars = array($myCar);



        // อันนี้ของจริงกำหนดระยะ 26ตัว สร้าง array assocมา
        foreach(range(0, 25) as $i) {
            $l=$i+1;
        $array[$i] = array('label' => "EB$l",
                            'icon' => 'view_headline',
                            'header' => 'generic',
                            'body' => 'toggle',
                            'children' => null,
                            );

        }
        
// -> ใช้เรียก object []ใช้เรียก array 
// เวลายัดค่าobjectใช้  {} array ก็[]
        
        foreach(range(1,count($product)) as $i) {

            // ตอนแรกคิดว่า พอมันเป็น 0 แล้วมันไม่ปรากฏเลขเลยเอาตัวนี้เผื่อไว้ให้มันloopยัดข้อมูลได้
            
            // if ($product[$i-1]['Type']-1 == 0) {
            //     $array[0]['children'][]=array('label' => $product[$i-1]['label'],
            //                 'header' => 'generic',
            //                 'body' => 'story',
            //                 'story' => $product[$i-1]['Date'],
            //                 'Path' => $product[$i-1]['Path'],
            //                 );
            // }else{
               $array[$product[$i-1]['Type'] - 1]['children'][]=array('label' => $product[$i-1]['label'],
                            'header' => 'generic',
                            'body' => 'story',
                            'story' => $product[$i-1]['Date'],
                            'Path' => $product[$i-1]['Path'],
                            ); 
            // }
        };
        
            
        return $response->withJson($array, 200);
    
});

?>
