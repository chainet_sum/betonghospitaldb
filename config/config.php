<?php
class db{
    private $host = "localhost";
    private $user = "root";
    private $pass = "";
    private $db = "vue_store";
    // ชุดล่างเป็นค่าของmysqlในserver
    // private $host = "localhost";
    // private $user = "cp517450_sum";
    // private $pass = "betong10749";
    // private $db = "cp517450_vue_store";

    public function connect(){
        try{ $mysql_connect_str = "mysql:host=$this->host;dbname=$this->db;charset=utf8";
        $dbConnect = new PDO($mysql_connect_str, $this->user, $this->pass);
        $dbConnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $dbConnect;
        }catch(PDOException $e){
            echo '{"error": {"text" : '.$e->getMessage().'}';
        }
       
    }
}

